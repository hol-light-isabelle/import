# Isabelle cheatsheet

```
declare [[ML_print_depth=1000]]
declare [[show_types=true]]
ML ‹ Thm.prop_of @{thm filter_def} ›
```

# HOL Light cheatsheet

```
# ALL;;
val it : thm = |- (ALL P [] <=> T) /\ (ALL P (CONS h t) <=> P h /\ ALL P t)
# concl ALL;;
val it : term = `(ALL P [] <=> T) /\ (ALL P (CONS h t) <=> P h /\ ALL P t)`
# frees (concl ALL);;
val it : term list = [`h`; `P`; `t`]
# hd (frees (concl ALL));;
val it : term = `h`
# dest_var (hd (frees (concl ALL)));;
val it : string * hol_type = ("h", `:?25356`)
# dest_var (hd (tl (frees (concl ALL))));;
val it : string * hol_type = ("P", `:?25356->bool`)
# dest_var (hd (tl (frees (concl ALL))));;
val it : string * hol_type = ("P", `:?25411->bool`)
# dest_var (hd (tl (frees (concl ALL))));;
val it : string * hol_type = ("P", `:?25411->bool`)
# CONJUNCT1 MAP;;
val it : thm = |- !f. MAP f [] = []
# concl (CONJUNCT1 MAP);;
val it : term = `!f. MAP f [] = []`
# dest_forall (concl (CONJUNCT1 MAP));;
val it : term * term = (`f`, `MAP f [] = []`)
# fst (dest_forall (concl (CONJUNCT1 MAP)));;
val it : term = `f`
# dest_var (fst (dest_forall (concl (CONJUNCT1 MAP))));;
val it : string * hol_type = ("f", `:A->B`)
```
